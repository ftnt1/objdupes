# README #

It finds duplicate objects within FortiOS config file (different names, same values)

## Files ##

### objdupes.py ###

This script prints all objects with different names but same values within a VDOM. It skips UUID's, comments.

Run it from within the directory containing firewall config(s).

It takes no command line arguments

### README.md ###

This file