#!/usr/bin/env python3

"""objdupes.py: It finds duplicate objects within FortiOS config file"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os

def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item


def sort_and_deduplicate(l):
    return list(uniq(sorted(l)))


def printnumlines(lst, s=0):
    for n, item in enumerate(lst):
        print(str(n+s) + ' - ' + str(item))


def hasvdoms():
    ans = 0
    for _ in cfg:
        if _ == 'config vdom\n':
            ans = 1
            break
    return ans


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal and displaying welcome message
print(chr(27) + "[2J" + GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS Duplicate Object Finder v1.0\n' + RED + 'Run "objdupes.py -h" for help with options\n' + NORM)


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()
print('\nDuplicate objects in ' + cfgfile + ':\n')

# slicing config file to start with 'config global\n' which will ease the vdom separation

hasvd = hasvdoms()
if hasvd:
    confstartindex = 0
    for num, line in enumerate(cfg):
        if line == 'config global\n':
            confstartindex = num
    cfg = cfg[confstartindex:]

# separating object definition parts of config file
# finding first line of object configuration part
objectstartindex = []
objectendindex = []
vdomindex = []


for num, line in enumerate(cfg):
    if hasvd:
        if line == 'config vdom\n':
            vdomindex.append(num)
    if line == 'config firewall address\n':
        objectstartindex.append(num)

# finding last line of object definition configuration part
for index in objectstartindex:
    for num, line in enumerate(cfg):
        if line == 'end\n' and num > index:
            objectendindex.append(num)
            break

for count, ind in enumerate(objectstartindex):
    if hasvd:
        vdom = (cfg[vdomindex[count] + 1][5:].strip('\n'))
        print('\n' + vdom)

    start = objectstartindex[count]
    end = objectendindex[count]

    # assigning object definition table slice to a list
    objectstable = cfg[start:end + 1]

    # slicing objectstable
    objstart = []
    objend = []
    tempobjtable = []
    objtable = []

    # finding first lines of single object definition config
    for num, line in enumerate(objectstable):
        if line.startswith('    edit '):
            objstart.append(num)

    # finding last lines of single object definition config
    for num, line in enumerate(objectstable):
        if line.startswith('    next'):
            objend.append(num)
    # removing UUID's and comments
    for num, line in enumerate(objstart):
        objtable.append(objectstable[objstart[num]:objend[num]])
    objtable = [[line for line in obj if not line.startswith('        set comment "') and not line.startswith('        set uuid ')] for obj in objtable]
    # deduplicating and printing the results
    dupelist = []
    for l1 in objtable:
        dupes = []
        for l2 in objtable:
            if l2[0] != l1[0]:
                if l2[1:] == l1[1:]:
                    dupes.append(l1[0][9:].strip('\n'))
                    dupes.append(l2[0][9:].strip('\n'))
                    dupelist.append(set(dupes))
    dupelist = sort_and_deduplicate(dupelist)


    printnumlines(dupelist, 1)
print('\n\n')